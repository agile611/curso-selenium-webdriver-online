# Curso de Selenium WebDriver

Código del curso de Selenium WebDriver de [Agile611](https://www.agile611.com)

## Pre-requisite:
1. MacOS, Linux o Windows
2. [Java](https://www.oracle.com/technetwork/java/javase/downloads/index.html)
3. [Maven](https://maven.apache.org/download.cgi)
4. [Firefox](https://www.mozilla.org/en-US/firefox/new/) instalado
5. [Chrome](https://www.google.com/chrome/) instalado
6. [Intellij Idea](https://www.jetbrains.com/idea/) o el IDE de Java que más os guste


Puedes encontrar el [curso completo aquí](https://www.agile611.com/cursos/agile/curso-online-selenium-webdriver-version-java/)

## Soporte

Este tutorial es de dominio público y lo ha realizado [Agile611](https://www.agile611.com) under WTFPL.

[![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-1.png)](http://www.wtfpl.net/)

Este README lo ha hecho [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhs/) y también es de dominio público.

Podéis contactar a [Agile611](https://www.agile611.com) por [aquí](https://www.agile611.com/formulario-de-contacto/) para más detalles.
